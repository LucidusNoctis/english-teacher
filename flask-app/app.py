from flask import Flask, render_template, request, url_for, redirect
from models import SessionLocal, hash_password, User, Word
from celery import Celery
from sqlalchemy.sql.expression import func
from random import shuffle

true_id = None
order = None
score = 0

app = Flask(__name__)

celery_app = Celery('celery_worker', broker='redis://redis:6379/0', backend='redis://redis:6379/0')
db = SessionLocal()



@app.route("/")
def start():
    r = celery_app.send_task('tasks.add', kwargs={'x': 1, 'y': 2})
    return render_template('start.html')


@app.route("/register", methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        login = request.form.get('login')
        password = request.form.get('password')
        hashed_password = hash_password(password)

        new_user = User(login=login, password=hashed_password)
        db.add(new_user)
        db.commit()
        db.close()
        return redirect(url_for("login"))
		
    return render_template("register.html")


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        login = request.form.get('login')
        password = request.form.get('password')
        
        db = SessionLocal()
        user = db.query(User).filter_by(login=login).first()
        db.close()
        
        if user and user.password == hash_password(password):
            return redirect(url_for("main"))

    return render_template('login.html')

@app.route('/main', methods=['GET', 'POST'])
def main():
    message = None
    if request.method == 'POST':
        new_word = request.form.get('new-word')
        translation = request.form.get('translation')
        message = 'Слово ' + new_word + ' додано у словник!'
        word = Word(word=new_word, translation=translation)
        db.add(word)
        db.commit()
        db.close()
        
    return render_template('main.html', message=message)

@app.route('/show_table')
def show_table():

    words = db.query(Word).all()

    return render_template('words-table.html', words=words)

@app.route('/task_1', methods=['GET', 'POST'])
def task_1():
    if request.method == 'POST':
        answer = request.form.get('choice')
        score += order[answer] == true_id if 1 else -1
        redirect(url_for("task_1"))
    random_words = db.query(Word).order_by(func.random()).limit(4).all()
    word = random_words[0].word
    true_id = random_words[0].id 
    shuffle(random_words)

    translate1 = random_words[0].translation
    translate2 = random_words[1].translation
    translate3 = random_words[2].translation
    translate4 = random_words[3].translation
    order = [word.id for word in random_words]

    
    return render_template('task-1.html', word=word, translate1=translate1,
                           						   translate2=translate2,
                                                   translate3=translate3,
                                                   translate4=translate4,
                                                   score="---")


if __name__=="__main__":
	app.run("0.0.0.0", debug=True)

