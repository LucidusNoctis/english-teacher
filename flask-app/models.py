from sqlalchemy import create_engine
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy import  Column, Integer, String
from sqlalchemy.orm import sessionmaker, Mapped, mapped_column
from hashlib import sha256


class Base(DeclarativeBase): pass


class User(Base):
    __tablename__ = "users"
  
    id: Mapped[int] = mapped_column(primary_key=True)
    login: Mapped[str] = mapped_column(String(100))
    password: Mapped[str] = mapped_column(String(100))

class Word(Base):
    __tablename__ = "words"
  
    id: Mapped[int] = mapped_column(primary_key=True)
    word: Mapped[str] = mapped_column(String(100))
    translation: Mapped[str] = mapped_column(String(100))

def hash_password(password):
    return sha256(password.encode()).hexdigest()    


sqlite_database = "mysql+mysqldb://root:testpass@webapp_db/qlanlearn"

engine = create_engine(sqlite_database)

Base.metadata.create_all(engine)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

